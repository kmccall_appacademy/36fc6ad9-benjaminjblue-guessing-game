# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  num = rand(100) + 1
  flag = false
  count = 0
  while !flag
    puts "guess a number"
    guess = gets.chomp.to_i
    if guess < num
      puts "#{guess} was too low"
    elsif guess > num
      puts "#{guess} was too high"
    else
      flag = true
    end
    count += 1
  end
  puts "Congrats, the number #{num} was correct! It took you #{count} tries."
end


def file_shuffle
  new_file = File.open("thing.txt", "w") do |file|
    file.write("hi")
  end
  puts "enter file name"
  file_name = gets.chomp
  file = File.open("#{file_name}.txt", "r")
  contents = file.readlines("movie-times.txt")
  contents = contents.shuffle
  file.close
  new_file = File.open("#{file_name}-shuffled.txt", "w") do |file|
    contents.each do |line|
      file.write(line)
    end
  end
end
